<?php

return [
    'migrate' => [
        [
            'command' => 'csv',
            'action' => 'ECommerce/Command/Migrate@run',
        ],
    ],
    'datasource' => [
        [
            'command' => 'fix-ip',
            'action' => 'ECommerce/Command/FixIp@run',
        ],
        [
            'command' => 'migrate',
            'action' => 'ECommerce/Command/DatasourceMigrate@run',
        ]
    ]
];
