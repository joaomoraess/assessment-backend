var depends = [];

depends.push('vue');
depends.push('component-header');
depends.push('component-footer');

define(depends, function(Vue) {
    return Vue.component('PageCategoryCreate', {
        template: '#page-category-create',
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                category: {
                    id: null,
                    name: null,
                    code: null,
                    slug: null
                },
                error: {
                    name: false,
                    code: false
                },
                errors: []
            };
        },
        mounted: function() {
            var self = this;
            if (this.$route.params.slug) {
                this.request('get', 'category/' + this.$route.params.slug, function(count, data) {
                    self.category = data[0];
                });
            }
        },
        methods: {
            save: function() {
                var self = this;

                var rules = [{
                    value: this.category.name,
                    rule: 'string',
                    required: true,
                    fail: function() {
                        self.error.name = true;
                    },
                    success: function(value) {
                        self.error.name = false;
                        self.category.name = value;
                    }
                },{
                    value: this.category.code,
                    rule: 'string',
                    required: true,
                    fail: function() {
                        self.error.code = true;
                    },
                    success: function(value) {
                        self.error.code = false;
                        self.category.code = value;
                    }
                }];

                var data = {
                    name: this.category.name,
                    code: this.category.code
                };

                if (!this.validate(rules)) {
                    var slug = this.$route.params.slug;
                    var path = 'category';
                    var method = 'post';
                    if (slug) {
                        path += '/' + slug;
                        method = 'put';
                    }
                    this.request(method, path, function(count, data) {
                        self.$router.push('/categories')
                    }, data);
                }
            }
        }
    });
});
