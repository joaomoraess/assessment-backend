var depends = [];
depends.push('vue');

define(depends, function(Vue) {
    Vue.mixin({
        data: function() {
            return {
                listeners: {}
            };
        },
        methods: {
            on: function(event) {
                if (this.bus && !this.listeners[event]) {
                    var self = this;
                    this.listeners[event] = function(params) {
                        if (self[event]) self[event](params);
                    };
                    this.bus.$on(event, this.listeners[event]);
                }
            },
            emit: function(event, params) {
                if (this.bus) {
                    this.bus.$emit(event, params);
                }
            }
        },
        beforeDestroy: function() {
            for (var e in this.listeners) {
                if (this.listeners.hasOwnProperty(e)) {
                    this.bus.$off(e, this.listeners[e]);
                }
            }
        }
    });
});
