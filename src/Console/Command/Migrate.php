<?php

namespace ECommerce\Console\Command
{
    class Migrate extends Command
    {
        public function run()
        {
            $params = $this->data();
            $file = current($params);
            $output = end($params);
            if($output === $file) {
                $output = null;
            }
            if (substr($file, 0, 1) != '/' && substr($file, 0, 1) != '~') {
                $file = getcwd() . '/' . $file;
            }
            if (is_file($file)) {
                $content = file_get_contents($file);
                $lines = explode("\n", $content);
                unset($lines[0]);
                if(isset($lines[count($lines)]) && empty($lines[count($lines)])) {
                    unset($lines[count($lines)]);
                }

                // begin transaction

                $count = 0;
                $total = count($lines);
                echo __("Lines to process: %s\n", $total);

                $categoriesService = $this->getService('ECommerce/Services/Categories');
                $productsService = $this->getService('ECommerce/Services/Products');

                $errorLine = [];
                $duplicateCategoriesLine = [];
                $duplicateProductsLine = [];
                $categoriesError = [];
                $productNonCategory = [];
                $successLines = [];

                try {
                    foreach($lines as $line) {
                        $count++;
                        $columns = explode(';', $line);

                        $name = isset($columns[0]) ? $columns[0] : null;
                        $sku = isset($columns[1]) ? $columns[1] : null;
                        $description = isset($columns[2]) ? $columns[2] : null;
                        $quantity = (int) isset($columns[3]) ? $columns[3] : null;
                        $price = (float) isset($columns[4]) ? $columns[4] : null;
                        $categories = explode('|', isset($columns[5]) ? $columns[5] : '');

                        $processed = number_format(($count * 100) / $total, 1);
                        echo __("\r                                         ", $processed);
                        echo __("\rProcessed %s%% line: %s/%s", $processed, $count, $total);

                        // save categorias
                        if(count($categories) > 0 && $categories[0] != '') {
                            $categories = $categoriesService->saveFromImport($categories);
                            if (count($categories['duplicates']) > 0) {
                                if(!isset($duplicateCategoriesLine[$count])) {
                                    $duplicateCategoriesLine[$count] = [];
                                }
                                $duplicateCategoriesLine[$count][] = $categories['duplicates'];
                            }
                            if (count($categories['errors']) > 0) {
                                if(!isset($errorLine[$count])) {
                                    $errorLine[$count] = [];
                                }
                                $errorLine[$count][] = $categories['errors'];
                            }
                            $categories = $categories['categoriesList'];
                        } else {
                            $categories = [];
                            $productNonCategory[$count] = $line;
                        }

                        // save product
                        $product = $productsService->saveFromImport(compact('name', 'sku', 'description', 'quantity', 'price', 'categories'));
                        
                        if(count($product['errors']) > 0) {
                            if(!isset($errorLine[$count])) {
                                $errorLine[$count] = [];
                            }
                            $errorLine[$count][] = $product['errors'];
                        }
                        
                        if(count($product['duplicates']) > 0) {
                            $duplicateProductsLine[$count] = $product['duplicates'];
                        }
                        
                        if(count($product['success']) > 0) {
                            $successLines[$count] = $product['success'];
                        }
                    }
                    echo __("\r                                         ", $processed);
                    echo __("\r".'Lines with error: %s', count($errorLine));
                    echo __("\n".'Lines processed: %s', $count);
                    echo __("\n".'Lines non category: %s', count($productNonCategory));
                    echo __("\n".'Lines product duplicate: %s', count($duplicateProductsLine));
                    echo __("\n".'Lines product success: %s', count($successLines));

                    if ($output) {
                        $printError = ['line;value;error'];
                        foreach($errorLine as $line => $fields) {
                            foreach($fields as $field) {
                                foreach($field as $key => $value) {
                                    foreach($value as $message) {
                                        $printError[] = $line . ';' . $key . ';' . $message;
                                    }
                                }
                            }
                        }
                        $printDuplicate = ['line;sku;name'];
                        foreach ($duplicateProductsLine as $line => $value) {
                            $value = current($value);
                            $printDuplicate[] = $line . ';' . $value['sku'] . ';' . $value['name'];
                        }
                        echo "\n";
                        $this->log('error-lines', $output, $printError);
                        $this->log('duplicate-lines', $output, $printDuplicate);
                        echo "\n\n";
                    }
                } catch (Exception $e) {
                }
            }
        }

        public function log(string $label, string $output, array $content)
        {
            $file = $label .'.' . $output;
            $fopen = fopen($file, 'w');
            fwrite($fopen, implode("\n", $content));
            fclose($fopen);
            echo __("\n".'Log file: %s', $file);
        }
    }
}