<?php

define('APP_DIR', __DIR__);
define('ECOMMERCE_CONTEXT', APP_DIR . '/../src/Config/context.php');
define('DATASOURCE_MIGRATION_PATH', APP_DIR . '/../infra/migrations');
define('UPLOAD_IMAGE_PATH', APP_DIR . '/../var/upload/');
define('CACHE_PATH', APP_DIR . '/../var/cache/');
define('PROVIDER_AUTOLOAD_PATH', APP_DIR . '/../*/Config');
define('FRONTEND_PATH', APP_DIR . '/../src/Frontend/');
define('FRONTEND_TEMPLATE_PATH', APP_DIR . '/../src/Frontend/Layout/template');
define('FRONTEND_AUTOLOAD_PATH', APP_DIR . '/../*/Config');
define('COMMAND_AUTOLOAD_PATH', APP_DIR . '/../*/Config');
define('LOGS_PATH', APP_DIR . '/../var/logs/');

require(APP_DIR . '/../vendor/autoload.php');

$di = \FcPhp\Di\Facades\DiFacade::getInstance();

$di->set('ECommerce/Core', 'ECommerce\Core\Core');
$di->make('ECommerce/Core', compact('di'))
    ->load()
    ->run();