<script id="page-category-create" type="text/x-template">
    <div>
        <component-header></component-header>
        <main class="content">
            <h1 class="title new-item" v-show="category.id == null"><?= __('New Category'); ?></h1>
            <h1 class="title new-item" v-show="category.id != null"><?= __('Edit Category'); ?></h1>
            <div class="input-field">
                <label for="category-name" class="label"><?= __('Category Name'); ?></label>
                <input type="text" id="category-name" class="input-text" v-model="category.name" :class="{'error': error.name}" />
            </div>
            <div class="input-field">
                <label for="category-code" class="label"><?= __('Category Code'); ?></label>
                <input type="text" id="category-code" class="input-text" v-model="category.code" :class="{'error': error.code}" />
            </div>
            <div class="errors">
                <ul>
                    <li v-for="error in errors"><?= __('"%s" on field "%s"', '{{ error.message }}', '{{ error.label }}') ?></li>
                </ul>
            </div>
            <div class="actions-form">
                <router-link to="/categories" class="action back"><?= __('Back'); ?></router-link>
                <button class="btn-submit btn-action" @click="save"><?= __('Save'); ?></button>
            </div>
        </main>
        <component-footer></component-footer>
    </div>
</script>