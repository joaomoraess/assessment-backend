<script id="page-product-create" type="text/x-template">
    <div>
        <component-header></component-header>
        <main class="content">
            <h1 class="title new-item" v-show="product.id == null"><?= __('New Product'); ?></h1>
            <h1 class="title new-item" v-show="product.id != null"><?= __('Edit Product'); ?></h1>
            <div class="input-field">
                <label for="name" class="label"><?= __('Product Name'); ?></label>
                <input type="text" id="name" class="input-text" v-model="product.name" :class="{'error': error.name}" /> 
            </div>
            <div class="input-field">
                <label for="sku" class="label"><?= __('Product SKU'); ?></label>
                <input type="text" id="sku" class="input-text" v-model="product.sku" :class="{'error': error.sku}" /> 
            </div>
            <div class="input-field">
                <label for="price" class="label"><?= __('Product Price'); ?></label>
                <money v-model="product.price" class="input-text" v-bind="{decimal: '<?= __('.'); ?>',thousands: '<?= __(','); ?>',prefix: '<?= __('US$'); ?> ',suffix: '',precision: 2,masked: false}" :class="{'error': error.price}"></money>
            </div>
            <div class="input-field">
                <label for="quantity" class="label"><?= __('Product Quantity'); ?></label>
                <input type="number" id="quantity" class="input-text" v-model.number="product.quantity" :class="{'error': error.quantity}" />
            </div>
            <div class="input-field">
                <label for="categories" class="label"><?= __('Product Categories'); ?></label>

                <multiselect v-model="product.categories" placeholder="<?= __('Select categories'); ?>" label="name" track-by="id" :options="categories" :multiple="true" :taggable="true"></multiselect>
            </div>
            <div class="input-field">
                <label for="description" class="label"><?= __('Product Description'); ?></label>
                <textarea id="description" class="input-text" v-model="product.description" :class="{'error': error.description}"></textarea>
            </div>
            <div class="input-field">
                <label for="description" class="label" v-show="product.images == undefined"><?= __('Product Image'); ?></label>
                <span v-if="product.images != undefined && product.images[0] != undefined">
                    <a :href="'/uploads/product/image/S/' + product.images[0].name" target="_blank"><?= __('Open Small') ?></a><br>
                    <a :href="'/uploads/product/image/M/' + product.images[0].name" target="_blank"><?= __('Open Medium') ?></a><br>
                    <a :href="'/uploads/product/image/L/' + product.images[0].name" target="_blank"><?= __('Open Large') ?></a><br>
                    <a :href="'/uploads/product/image/R/' + product.images[0].name" target="_blank"><?= __('Open Real') ?></a><br>
                    <img :src="'/uploads/product/image/S/' + product.images[0].name" />
                </span>
                <span v-else>
                    <input type="file" @change="configureImage" />
                </span>
            </div>
            <div class="errors">
                <ul>
                    <li v-for="error in errors"><?= __('"%s" on field "%s"', '{{ error.message }}', '{{ error.label }}') ?></li>
                </ul>
            </div>
            <div class="actions-form">
                <router-link to="/products" class="action back"><?= __('Back'); ?></router-link>
                <button class="btn-submit btn-action" @click="save"><?= __('Save'); ?></button>
            </div>
        </main>
        <component-footer></component-footer>
    </div>
</script>