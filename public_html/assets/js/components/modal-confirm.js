var depends = [];
depends.push('vue');

define(depends, function(Vue) {
    return Vue.component('ModalConfirm', {
        template: '#component-modal-confirm'
    });
});
