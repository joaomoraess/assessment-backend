var depends = [];
depends.push('vue');
depends.push('accounting');

define(depends, function(Vue, Accounting) {
    Vue.filter('currency', function (price, currency, lang) {
        if(!currency) {
            currency = 'US$';
        }
        if(!lang) {
            lang = 'en-us';
        }
        if (lang == 'en-us') {
            var mid = ",";
            var dec = ".";
        }
        if (lang == 'pt-br') {
            var mid = ".";
            var dec = ",";
        }
        return Accounting.formatMoney(parseFloat(price), currency + ' ', 2, mid, dec);
    });
});
