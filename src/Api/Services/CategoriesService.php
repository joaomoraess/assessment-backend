<?php

namespace ECommerce\Api\Services
{
    use Exception;

    class CategoriesService extends Service
    {
        public function findAll()
        {
            return $this
                ->getRepository('ECommerce/Repositories/Categories')
                ->find();
        }

        public function store(string $slug = null)
        {
            $id = null;
            $name = $this->data('name');
            $code = $this->data('code');

            $model = $this->getModel('ECommerce/Models/Category');

            if ($slug) {
                $old = $this->findOne($slug);
                if (count($old['data']) == 1) {
                    $old = current($old['data']);
                    $id = $old->getId();
                    $model->populate(compact('id', 'name', 'code'));
                } else {
                    // $this->log->error('Category Update: not found slug[' . $slug . ']');
                    return $this->messageError(__('Try update category but not found'));
                }
            } else {
                $model->populate(compact('name', 'code'));
            }
            if ($model->validate()) {
                $model = $this
                    ->getRepository('ECommerce/Repositories/Categories')
                    ->store($model);
                $this->logSuccessModel('Category ' . (isset($old) ? 'Edit' : 'Create'), $model['data'][0], $old);
                return $model;
            }
            $error = $model->error();
            $this->logErrorModel('Category ' . ($model->getId() > 0 ? 'Edit' : 'Create'), $error['error']);
            return $error;
        }

        public function findOne(string $term)
        {
            return $this
                ->getRepository('ECommerce/Repositories/Categories')
                ->findByIdOrSlug($term);
        }

        public function findByName(string $name)
        {
            return $this
                ->getRepository('ECommerce/Repositories/Categories')
                ->findByName($name);
        }

        public function delete(string $term)
        {
            $category = $this->findOne($term);
            if (count($category['data']) == 1) {
                try {
                    $model = current($category['data']);
                    $this
                        ->getRepository('ECommerce/Repositories/Categories')
                        ->delete($model);
                    $this->logSuccessModel('Category Delete', $model);
                    return $category;
                } catch (Exception $e) {
                    $message = __('Exception on delete category! Try again!');
                    // $this->log->error('Category Delete: Error on delete "' . $model->getName() . '"');
                    if (substr($e->getMessage(), 0, 15) == 'SQLSTATE[23000]') {
                        // $this->log->error('Category Delete: This category "' . $model->getName() . '" have products');
                        $message = __('This category have products!');
                    }
                    return $this->messageError($message);
                }
            } else {
                // $this->log->error('Category Delete: not found slug[' . $term . ']');
                return $this->messageError(__('Try delete category but not found'));
            }
        }

        public function saveFromImport(array $categories)
        {   
            $errors = [];
            $duplicates = [];
            $categoriesList = [];

            $categoriesRepository = $this->getRepository('ECommerce/Repositories/Categories');
            $categoriesRepository->beginTransaction();

            foreach ($categories as $key => $categoryName) {

                $model = $this->getModel('ECommerce/Models/Category');
                $model->setName($categoryName);
                $model->setCode('File Import');

                if($model->validate()) {
                    $unique = $this->findByName($model->getName());
                    if($unique['count'] == 0) {
                        $categoriesRepository->store($model);
                        $this->logSuccessModel('Category Create', $model);
                    }else {
                        $model = $unique['data'][0];
                        $duplicates[$model->getName()] = __('Category already exists');
                        // $this->log->error('Category "' . $model->getName() . '" already exists');
                    }
                }else{
                    $catError = [];
                    foreach ($model->error()['error'] as $error) {
                        $catError[] = $error['message'] . ': ' . $error['label'];
                    }
                    $errors[$model->getName()] = $catError;
                    $this->logErrorModel('Category ' . ($model->getId() > 0 ? 'Edit' : 'Create'), $model->error()['error']);
                }
                $categoriesList[$key] = $model->toArray();
            }
            return compact('errors', 'duplicates', 'categoriesList');
        }
    }
}