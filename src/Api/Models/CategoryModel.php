<?php

namespace ECommerce\Api\Models
{
    class CategoryModel extends Model
    {
        protected $fields = [
            'id',
            'name',
            'code',
            'slug'
        ];

        protected $validate = [
            'id' => [
                'type' => 'int',
                'size' => 11,
            ],
            'name' => [
                'type' => 'string',
                'format' => 'default',
                'size' => 30,
                'required' => true,
            ],
            'code' => [
                'type' => 'string',
                'format' => 'default',
                'size' => 30,
                'required' => true,
            ],
            'slug' => [
                'type' => 'string',
                'format' => 'slug',
                'size' => 30,
                'populate' => ['id', 'name']
            ],
        ];

        public function modify()
        {
            $this->validate['name']['label'] = __('Category Name');
            $this->validate['code']['label'] = __('Category Code');
        }
    }
}