<?php

namespace ECommerce\Console\Command
{
    use FcPhp\Di\Interfaces\IDi;
    use ECommerce\Api\Traits\Flow;

    class Command
    {
        use Flow;

        private $di;
        private $data;

        public function __construct(IDi $di)
        {
            $this->di = $di;
        }

        public function setData(array $data)
        {
            $this->data = $data;
            return $this;
        }

        public function data(string $key = null)
        {
            if (isset($this->data[$key])) {
                return $this->data[$key];
            }
            if(empty($key)) {
                return $this->data;
            }
            return null;
        }
    }
}