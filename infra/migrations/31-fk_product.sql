ALTER TABLE `productCategory`
    ADD CONSTRAINT `fk_product`
    FOREIGN KEY (`product_id`)
    REFERENCES `product`(`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT;