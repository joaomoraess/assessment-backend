<?php

namespace ECommerce\Config\Providers
{
    use FcPhp\Di\Interfaces\IDi;
    use FcPhp\Provider\Interfaces\IProviderClient;

    class ECommerce implements IProviderClient
    {
        /**
         * Method to configure Di in providers
         *
         * @param FcPhp\Di\Interfaces\IDi $di Di Instance
         * @return void
         */
        public function getProviders(IDi $di) :IDi
        {
            $context = $di->make('FcPhp/Context');
            $log = $di->get('FcPhp/Log');

            // API
            $di->set('ECommerce/Api/View', 'ECommerce\Api\View');
            $di->set('ECommerce/Api', 'ECommerce\Api\Api', [
                'di' => $di,
                'view' => $di->get('ECommerce/Api/View'),
            ]);

            // Categories Model/Repository/Service
            $di->setNonSingleton('ECommerce/Models/Category', 'ECommerce\Api\Models\CategoryModel');
            $di->set('ECommerce/Repositories/Categories', 'ECommerce\Api\Repositories\CategoriesRepository', [
                'di' => $di,
                'datasource' => $di->get('ECommerce/Datasources/MySQL')
            ]);
            $di->set('ECommerce/Services/Categories', 'ECommerce\Api\Services\CategoriesService', [
                'di' => $di,
                'log' => $log
            ]);

            // Products Model/Repository/Service
            $di->setNonSingleton('ECommerce/Models/Product', 'ECommerce\Api\Models\ProductModel');
            $di->set('ECommerce/Repositories/Products', 'ECommerce\Api\Repositories\ProductsRepository', [
                'di' => $di,
                'datasource' => $di->get('ECommerce/Datasources/MySQL')
            ]);
            $di->set('ECommerce/Services/Products', 'ECommerce\Api\Services\ProductsService', [
                'di' => $di,
                'log' => $log
            ]);

            // Product Category Model/Repository
            $di->setNonSingleton('ECommerce/Models/ProductCategory', 'ECommerce\Api\Models\ProductCategoryModel');
            $di->set('ECommerce/Repositories/ProductsCategories', 'ECommerce\Api\Repositories\ProductsCategoriesRepository', [
                'di' => $di,
                'datasource' => $di->get('ECommerce/Datasources/MySQL')
            ]);

            // Product Image Model/Repository
            $di->setNonSingleton('ECommerce/Models/ProductImage', 'ECommerce\Api\Models\ProductImageModel');
            $di->set('ECommerce/Repositories/ProductsImages', 'ECommerce\Api\Repositories\ProductsImagesRepository', [
                'di' => $di,
                'datasource' => $di->get('ECommerce/Datasources/MySQL')
            ]);
            $di->set('ECommerce/Services/ProductsImages', 'ECommerce\Api\Services\ProductsImagesService', [
                'di' => $di,
                'log' => $log
            ]);

            // Frontend
            $di->set('ECommerce/Frontend/View', 'ECommerce\Frontend\View', []);
            $di->set('ECommerce/Frontend', 'ECommerce\Frontend\Frontend', [
                'view' => $di->get('ECommerce/Frontend/View', [
                    'path' => FRONTEND_PATH
                ]),
                'layout' => FRONTEND_TEMPLATE_PATH
            ]);

            // Console
            $di->set('ECommerce/Console', 'ECommerce\Console\Console', [
                'di' => $di
            ]);
            $di->set('ECommerce/Command/Migrate', 'ECommerce\Console\Command\Migrate', [
                'di' => $di
            ]);
            $di->set('ECommerce/Command/FixIp', 'ECommerce\Console\Command\FixIp', [
                'di' => $di
            ]);
            $di->set('ECommerce/Command/DatasourceMigrate', 'ECommerce\Console\Command\DatasourceMigrate', [
                'di' => $di,
                'datasource' => $di->get('ECommerce/Datasources/MySQL')
            ]);

            return $di;
        }
    }
}