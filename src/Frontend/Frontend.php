<?php

namespace ECommerce\Frontend
{
    use ECommerce\Frontend\Interfaces\IView;

    class Frontend
    {
        public function __construct(IView $view, string $layout)
        {
            $this->view = $view;
            $this->layout = $layout;
        }

        public function run()
        {
            $this->view->render($this->layout);
        }
    }
}
