# App ECommerce

Para realizar a instalação, esteja em um computador Debian/Ubuntu e certifique-se de ter acesso ao usuário root (será solicitado senha sudo).

Dependências:

- [Make Unix](https://pt.wikipedia.org/wiki/Make)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

Caso não possua instalado [Docker](https://www.docker.com/) e [Docker Compose](https://docs.docker.com/compose/), execute (somente para: Ubuntu Bionic 18.04 (LTS)):

```
$ make server-install
```

#### Instalação:

1) Não é necessário instalar o PHP em sua máquina, todo processo de instalação é executado através do container Docker. Execute:

```
$ make infra-install
```

2) Para instalar a aplicação e suas dependências via "composer", "npm" e realizar o "migrate" do banco de dados, execute:

```
$ make app-install infra-setup
```

#### Iniciar ambiente:

```
$ make infra
```

#### Finalizar ambiente:

```
$ make infra-down
```

#### Visualizar aplicação:

Acesse em seu navegador: [http://localhost/](http://localhost/)

# Console

#### Migrar arquivo CSV:

No terminal, no diretório do app execute:

```
$ bin/console migrate csv path/to/import.csv out.csv
```

Após será gerado arquivos CSV com relatório de erros e duplicados, conforme abaixo:

Retorno:
```
Lines to process: 35
Lines with error: 0                      
Lines processed: 35
Lines non category: 1
Lines product duplicate: 35
Lines product success: 0

Log file: error-lines.out.csv
Log file: duplicate-lines.out.csv

```

#### Alterar IP MySQL (context.php)

Para ajustar automaticamente de acordo com o IP do container Docker:

```
$ make fix-ip
```

#### Migrate banco de dados

Para realizar o migrate do banco de dados:

```
$ bin/console datasource migrate
```

# Tecnologias

![Image Flow App](flow.jpg?raw=true "Image Flow App")

### Back end

Open source:
- [PHP](http://php.net/)
- [MySQL](https://www.mysql.com/)
- [Docker](https://www.docker.com/)
- [Apache](https://www.apache.org/)
- [Composer](https://getcomposer.org/)

Packages autorais que utilizo neste app:
- [FcPhp Command](https://github.com/00F100/fcphp-command) Manipulate and verify console commands
- [FcPhp Context](https://github.com/00F100/fcphp-context) Provides context configuration to app
- [FcPhp Di](https://github.com/00F100/fcphp-di) Dependency Injection
- [FcPhp i18n](https://github.com/00F100/fcphp-i18n) Translate package
- [FcPhp Log](https://github.com/00F100/fcphp-log) Manipulate logs
- [FcPhp Provider](https://github.com/00F100/fcphp-provider) Provide classes to dependency injection
- [FcPhp Request](https://github.com/00F100/fcphp-request) Define if request is console or http
- [FcPhp Route](https://github.com/00F100/fcphp-route) Manipulate and verify http routes

Conceito:

- Frontend -> HTTP Request -> Api Data

- Api Data: Service -> Repository -> Model

- [Commands](https://bitbucket.org/joaomoraess/assessment-backend/src/master/src/Config/commands.php)
- [Contexts](https://bitbucket.org/joaomoraess/assessment-backend/src/master/src/Config/context.php)
- [Providers](https://bitbucket.org/joaomoraess/assessment-backend/src/master/src/Config/provider.php)
- [Routes](https://bitbucket.org/joaomoraess/assessment-backend/src/master/src/Config/routes.php)

### Front end

- [RequireJS](https://requirejs.org/)
- [Vue.js](https://vuejs.org/)

- [Routes](https://bitbucket.org/joaomoraess/assessment-backend/src/master/public_html/assets/js/app.js)
- [Pages JS](https://bitbucket.org/joaomoraess/assessment-backend/src/master/public_html/assets/js/pages/)
- [Pages Template](https://bitbucket.org/joaomoraess/assessment-backend/src/master/src/Frontend/Layout/Pages/)