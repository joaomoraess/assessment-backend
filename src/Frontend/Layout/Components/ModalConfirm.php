<script id="component-modal-confirm" type="text/x-template">
<transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header">
              default header
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
            </slot>
            <slot name="errors">
            </slot>
          </div>

          <div class="modal-footer">
            <slot name="confirm">
            </slot>
            <slot name="cancel">
            </slot>
          </div>
        </div>
      </div>
    </div>
</transition>
</script>