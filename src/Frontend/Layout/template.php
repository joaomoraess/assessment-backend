<html>
    <head>
        <title><?= __('ECommerce App'); ?></title>
        <link  rel="stylesheet" type="text/css" media="all" href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.css" />
        <link  rel="stylesheet" type="text/css" media="all" href="/assets/node_modules/vue-multiselect/dist/vue-multiselect.min.css" />
        <link  rel="stylesheet" type="text/css" media="all" href="/assets/css/style.css" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
        <?= $this->vueTemplate([
            'Components/Header',
            'Components/Footer',
            'Components/ModalConfirm',
            'Pages/Error',
            'Pages/Products',
            'Pages/Dashboard',
            'Pages/ProductCreate',
            'Pages/ProductView',
            'Pages/CategoryCreate',
            'Pages/Categories',
        ]); ?>
    </head>
    <body>
        <div id="app" v-cloak>
            <router-view></router-view>
        </div>

        <script type="text/javascript" src="/assets/node_modules/requirejs/require.js" data-main="/assets/js/bootstrap"></script>
    </body>
</html>