CREATE TABLE IF NOT EXISTS `productCategory` (
    `product_id` INT(11) NOT NULL ,
    `category_id` INT(11) NOT NULL,
    INDEX `product_category_index` (`product_id`, `category_id`)
) ENGINE = InnoDB;