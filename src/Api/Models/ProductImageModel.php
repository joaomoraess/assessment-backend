<?php

namespace ECommerce\Api\Models
{
    class ProductImageModel extends Model
    {
        protected $fields = [
            'product_id',
            'group',
            'name',
            'image',
            'size'
        ];

        protected $validate = [
            'product_id' => [
                'type' => 'int',
                'size' => 11,
            ],
            'group' => [
                'type' => 'int',
                'size' => 15,
            ],
            'name' => [
                'type' => 'string',
                'size' => 255,
            ],
            'image' => [
                'type' => 'string',
                'size' => 255,
            ],
            'size' => [
                'type' => 'enum',
                'options' => ['S', 'M', 'L', 'R'],
            ],
        ];

        public function modify()
        {
            $this->validate['product_id']['label'] = __('Product');
            $this->validate['group']['label'] = __('Image Group');
            $this->validate['name']['label'] = __('Image Name');
            $this->validate['image']['label'] = __('Image Path');
            $this->validate['image']['size'] = __('Image Size');
        }
    }
}