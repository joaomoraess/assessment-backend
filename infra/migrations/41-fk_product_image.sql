ALTER TABLE `productImage`
    ADD CONSTRAINT `fk_product_image`
    FOREIGN KEY (`product_id`)
    REFERENCES `product`(`id`)
        ON DELETE RESTRICT ON UPDATE RESTRICT;