CREATE TABLE IF NOT EXISTS `productImage` (
    `product_id` INT(11) NOT NULL,
    `group` INT(15) NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `image` VARCHAR(255) NOT NULL,
    `size` ENUM("S", "M", "L", "R") NOT NULL,
    INDEX `name_index` (`name`),
    INDEX `product_index` (`product_id`),
    INDEX `size_name_image_index` (`size`, `name`),
    INDEX `name_product_index` (`name`, `product_id`)
) ENGINE = InnoDB;