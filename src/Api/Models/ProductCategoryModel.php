<?php

namespace ECommerce\Api\Models
{
    class ProductCategoryModel extends Model
    {
        protected $fields = [
            'product_id',
            'category_id',
        ];

        protected $validate = [
            'product_id' => [
                'type' => 'int',
                'size' => 11,
            ],
            'category_id' => [
                'type' => 'int',
                'size' => 11,
            ],
        ];

        public function modify()
        {
            $this->validate['product_id']['label'] = __('Product');
            $this->validate['category_id']['label'] = __('Category');
        }
    }
}