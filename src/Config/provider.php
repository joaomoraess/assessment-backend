<?php

return [
    '\ECommerce\Config\Providers\Datasources',
    '\ECommerce\Config\Providers\Route',
    '\ECommerce\Config\Providers\Command',
    '\ECommerce\Config\Providers\Request',
    '\ECommerce\Config\Providers\ECommerce',
];