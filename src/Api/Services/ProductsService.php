<?php

namespace ECommerce\Api\Services
{
    use ECommerce\Api\Interfaces\Models\IModel;

    class ProductsService extends Service
    {
        public function findAll()
        {
            $currentPage = $this->queryString('currentPage');
            $perPage = $this->queryString('perPage');
            if (!$currentPage || !$perPage) {
                return $this->messageError(base64_decode('Q29tbyBkaXJpYSBvIEZhdXN0YW86IEVSUlJPT08h'));
            } else {
                if ($perPage > 100) {
                return $this->messageError(base64_decode('UXVhbCBvIHNldSBwcm9ibGVtYT8='));
                }
            }

            return $this
                ->getRepository('ECommerce/Repositories/Products')
                ->find(compact('currentPage', 'perPage'));
        }

        public function store(string $slug = null)
        {
            $id = null;
            $name = $this->data('name');
            $sku = $this->data('sku');
            $price = (float) $this->data('price');
            $quantity = (int) $this->data('quantity');
            $available = $this->data('available');
            $description = $this->data('description');
            $categories = $this->data('categories');
            $model = $this->getModel('ECommerce/Models/Product');

            $model->populate(compact('name', 'sku', 'price', 'quantity', 'available', 'description', 'categories'));
            
            if (!$model->validate()) {
                $error = $model->error();
                $this->logErrorModel('Product ' . (isset($old) ? 'Edit' : 'Create'), $error['error']);
                return $error;
            }

            if ($slug) {
                $old = $this->findOne($slug);
                if (count($old['data']) == 1) {
                    $old = current($old['data']);
                    $id = $old->getId();

                    $unique = $this->findBySku($sku, $id);
                    if (count($unique['data']) == 1) {
                        // $this->log->error('Product "' . $model->getSku() . '" already exists');
                        return $this->messageError(__('This record already exists'), 'sku');
                    }
                    $model->populate(compact('id', 'name', 'sku', 'price', 'quantity', 'available', 'description', 'categories'));
                } else {
                    // $this->log->error('Product Update: not found slug[' . $slug . ']');
                    return $this->messageError(__('Try update product but not found'));
                }
            } else {
                $unique = $this->findBySku($sku);
                if (count($unique['data']) == 1) {
                    // $this->log->error('Product Create: Already exists sku[' . $unique['data'][0]->getSku() . ']');
                    return $this->messageError(__('This record already exists'), 'sku');
                }
            }
            if ($model->validate()) {
                $model = $this->save($model);
                $this->logSuccessModel('Product ' . (isset($old) ? 'Edit' : 'Create'), $model['data'][0], $old);
                return $model;
            }
            $error = $model->error();
            $this->logErrorModel('Product ' . (isset($old) ? 'Edit' : 'Create'), $error['error']);
            return $error;
        }

        public function save(IModel $model)
        {
            $result = $this
                ->getRepository('ECommerce/Repositories/Products')
                ->store($model);

            $modelProductCategory = $this->getModel('ECommerce/Models/ProductCategory');
            $categoriesRepository = $this->getRepository('ECommerce/Repositories/ProductsCategories');

            $modelProductCategory->setProduct_Id($model->getId());
            $categoriesRepository->deleteByProduct($modelProductCategory);

            foreach($model->getCategories() as $category) {
                $modelProductCategory = $this->getModel('ECommerce/Models/ProductCategory');
                $modelProductCategory->setProduct_Id($model->getId());
                $modelProductCategory->setCategory_Id($category['id']);
                $categoriesRepository->store($modelProductCategory);
            }

            return $result;
        }

        public function findOne(string $term)
        {
            return $this
                ->getRepository('ECommerce/Repositories/Products')
                ->findByIdOrSlug($term);
        }

        public function findBySku(string $sku, int $id = null)
        {
            return $this
                ->getRepository('ECommerce/Repositories/Products')
                ->findBySku($sku, $id);
        }

        public function delete(string $term)
        {
            $product = $this->findOne($term);
            if (count($product['data']) == 1) {
                $model = current($product['data']);

                $modelProductCategory = $this->getModel('ECommerce/Models/ProductCategory');
                $categoriesRepository = $this->getRepository('ECommerce/Repositories/ProductsCategories');
                $modelProductCategory->setProduct_Id($model->getId());
                $categoriesRepository->deleteByProduct($modelProductCategory);

                $modelProductImage = $this->getModel('ECommerce/Models/ProductImage');
                $productsImageRepository = $this->getRepository('ECommerce/Repositories/ProductsImages');
                $modelProductImage->setProduct_Id($model->getId());
                $productsImageRepository->deleteByProduct($modelProductImage);
                
                $this
                    ->getRepository('ECommerce/Repositories/Products')
                    ->delete($model);
                $this->logSuccessModel('Product Delete', $model);
                return $product;
            } else {
                // $this->log->error('Product Delete: not found slug[' . $term . ']');
                return $this->messageError(__('Try delete product but not found'));
            }
        }

        public function image(string $term)
        {
            $images = [];
            $product = $this->findOne($term);
            $context = $this->getContext();
            $group = time();

            if (count($product['data']) == 1) {

                $product = current($product['data']);

                $tmpImage = $this->data('image');
                $fileExt = explode('.', $tmpImage['name']);
                $fileExt = end($fileExt);
                $model = $this->getModel('ECommerce/Models/ProductImage');
                $model->populate([
                    'product_id' => $product->getId(),
                    'group' => $group,
                    'name' => md5($tmpImage['tmp_name']) . '.' . $fileExt,
                    'image' => md5($tmpImage['name'] . time()) . '.' . $fileExt,
                    'size' => 'R',
                ]);

                $type = $tmpImage['type'];

                if($model->validate()) {
                    if (in_array($type, $context->get('uploadImage.headerEnable'))) {
                        $saveFile = UPLOAD_IMAGE_PATH . $model->getImage();
                        try {
                            move_uploaded_file($tmpImage['tmp_name'], $saveFile);
                            $productImageRepository = $this->getRepository('ECommerce/Repositories/ProductsImages');
                            $productImageRepository->store($model);
                            $images[] = $model;

                            foreach($context->get('uploadImage.sizes') as $label => $sizes) {
                                $width = $sizes[0];
                                $height = $sizes[1];

                                $size = getimagesize($saveFile);
                                $ext = $size['mime'];
                                switch($ext) {
                                    case 'image/jpg':
                                    case 'image/jpeg':
                                        $image = imagecreatefromjpeg($saveFile);
                                        break;
                                    case 'image/png':
                                        $image = @imagecreatefrompng($saveFile);
                                        break;
                                }
                                $origWidth = imagesx($image);
                                $origHeight = imagesy($image);

                                $ratio_orig = $origWidth/$origHeight;

                                if ($width/$height > $ratio_orig) {
                                    $width = $height*$ratio_orig;
                                } else {
                                    $height = $width/$ratio_orig;
                                }

                                $newImage = imagecreatetruecolor($width, $height);
                                imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $origWidth, $origHeight);

                                $model = $this->getModel('ECommerce/Models/ProductImage');
                                $model->populate([
                                    'product_id' => $product->getId(),
                                    'group' => $group,
                                    'name' => md5($tmpImage['tmp_name']) . '.' . $fileExt,
                                    'image' => md5($tmpImage['name'] . (time()*rand(375,82438))) . '.' . $fileExt,
                                    'size' => $label,
                                ]);

                                if($model->validate()) {
                                    $saveFile = UPLOAD_IMAGE_PATH . $model->getImage();
                                    switch($ext) {
                                        case 'image/jpg':
                                        case 'image/jpeg':
                                            imagejpeg($newImage, $saveFile, 100);
                                            break;
                                        case 'image/png':
                                            imagepng($newImage, $saveFile, 9);
                                            break;
                                    }
                                    $productImageRepository->store($model);
                                    $images[] = $model;
                                }
                            }
                            return [
                                'count' => count($images),
                                'data' => $images
                            ];
                        } catch (\Exception $e) {
                            return $this->messageError(__('Fail on save image into server'));
                        }
                    }
                    return $this->messageError(__('Invalid file type'));
                }
                return $model->error();
            }
        }

        public function saveFromImport(array $product)
        {
            $errors = [];
            $duplicates = [];
            $success = [];

            $model = $this->getModel('ECommerce/Models/Product');
            $model->populate($product);

            if($model->validate()) {
                $unique = $this->findBySku($model->getSku(), $model->getId());
                if (count($unique['data']) == 1) {
                    $unique = current($unique['data']);
                    $duplicates[] = $unique->toArray();
                } else {
                    $this->save($model);
                    $success[] = $model->toArray();
                }
            } else {
                $productError = [];
                foreach ($model->error()['error'] as $error) {
                    $productError[] = $error['message'] . ': ' . $error['label'];
                }
                $errors[$model->getSku()] = $productError;
            }
            
            return compact('errors', 'duplicates', 'success');
        }
    }
}