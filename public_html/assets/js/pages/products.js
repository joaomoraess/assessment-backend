var depends = [];

depends.push('vue');
depends.push('vuejs-paginate');
depends.push('component-header');
depends.push('component-footer');
depends.push('modal-confirm');
depends.push('currency');

define(depends, function(Vue, VuejsPaginate) {
    
    // Vue.use(BootstrapVue);
    return Vue.component('PageProducts', {
        template: '#page-products',
        components: {
            'paginate': VuejsPaginate,
        },
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                currentPage: 1,
                pages: 1,
                perPage: 10,
                count: 0,
                products: [],
                remove: {},
                showModalConfirm: false,
                errors: []
            };
        },
        mounted: function() {
            this.populate();
        },
        methods: {
            populate: function() {
                var self = this;
                this.request('get', 'products', function(count, data, pages) {
                    self.count = count;
                    self.products = data;
                    self.pages = pages;
                }, {}, this.currentPage, this.perPage);
            },
            deleteProduct: function(product) {
                this.remove = product;
                this.showModalConfirm = true;
            },
            deleteProductAction: function() {
                var self = this;
                this.request('delete', 'product/' + this.remove.id, function(count, data) {
                    self.showModalConfirm = false;
                    self.populate();
                });
            },
            changePage: function(currentPage) {
                this.currentPage = currentPage;
                window.scrollTo(0,200);
                this.populate();
            }
        }
    });
});
