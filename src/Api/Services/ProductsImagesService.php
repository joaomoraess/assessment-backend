<?php

namespace ECommerce\Api\Services
{
    use Exception;
    
    class ProductsImagesService extends Service
    {
        public function imagePrint(string $size, string $name)
        {
            $image = $this
                ->getRepository('ECommerce/Repositories/ProductsImages')
                ->imagePrint($size, $name);
            if($image['count'] == 1) {
                $image = current($image['data']);
                $context = $this->getContext();
                $this->getView()->renderImage(UPLOAD_IMAGE_PATH . $image['image']);
            }
            $this->getView()->error();
        }

        public function deleteFiles($data)
        {
            $context = $this->getContext();
            foreach($data as $value) {
                $file = UPLOAD_IMAGE_PATH . $value['image'];
                if(is_file($file)) {
                    try {
                        unlink($file);
                    } catch(Exception $e) {}
                }
            }
        }
    }
}