<?php

namespace ECommerce\Config\Providers
{
    use FcPhp\Di\Interfaces\IDi;
    use FcPhp\Provider\Interfaces\IProviderClient;

    class Datasources implements IProviderClient
    {
        /**
         * Method to configure Di in providers
         *
         * @param FcPhp\Di\Interfaces\IDi $di Di Instance
         * @return void
         */
        public function getProviders(IDi $di) :IDi
        {
            $context = $di->make('FcPhp/Context');
            $di->set('ECommerce/Datasources/MySQL', 'ECommerce\Api\Datasources\MySQL', [
                'host' => $context->get('datasource.mysql.host'),
                'port' => $context->get('datasource.mysql.port'),
                'database' => $context->get('datasource.mysql.database'),
                'username' => $context->get('datasource.mysql.username'),
                'password' => $context->get('datasource.mysql.password'),
            ]);
            return $di;
        }
    }
}