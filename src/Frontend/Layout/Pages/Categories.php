<script id="page-categories" type="text/x-template">
    <div>
        <component-header></component-header>
        <modal-confirm v-show="showModalConfirm">
            <h3 slot="header"><?= __('Confirm delete category?'); ?></h3>
            <p slot="body"><?= __('Category: %s', '{{remove.name}}'); ?></p>
            <p slot="body"><?= __('This action can not be undone'); ?></p>
            <p slot="body">
                <ul>
                    <li v-for="error in errors"><?= __('Have error on delete: %s', '{{ error.message }}') ?></li>
                </ul>
            </p>
            <button class="button-cancel" slot="cancel" @click="showModalConfirm = false; errors = []"><?= __('Cancel'); ?></button>
            <button class="button-confirm" slot="confirm" @click="deleteCategoryAction" :disabled="errors.length > 0"><?= __('Confirm'); ?></button>
        </modal-confirm>
        <main class="content">
            <div class="header-list-page">
                <h1 class="title"><?= __('Categories'); ?></h1>
                <router-link to="/category/create" class="btn-action"><?= __('Add new Category'); ?></router-link>
            </div>
            <table class="data-grid">
                <thead>
                    <tr class="data-row">
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content"><?= __('Name'); ?></span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content"><?= __('Code'); ?></span>
                        </th>
                        <th class="data-grid-th">
                            <span class="data-grid-cell-content"><?= __('Actions'); ?></span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="data-row" v-for="category in categories">
                        <td class="data-grid-td">
                           <span class="data-grid-cell-content">{{category.name}}</span>
                        </td>
                        <td class="data-grid-td">
                           <span class="data-grid-cell-content">{{category.code}}</span>
                        </td>
      
                        <td class="data-grid-td">
                            <div class="actions">
                                <div class="action edit">
                                    <router-link :to="'/category/edit/' + category.slug"><span>Edit</span></router-link>
                                </div>
                                <div class="action delete">
                                    <a href="javascript:;" @click="deleteCategory(category)"><span>Delete</span></a>
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </main>
        <component-footer></component-footer>
    </div>
</script>