<?php

namespace ECommerce\Api\Repositories
{
    use ECommerce\Api\Interfaces\Models\IModel;

    class CategoriesRepository extends Repository
    {
        public function find(array $options = [])
        {
            $data = $this->select('
                SELECT
                    `c`.`id`,
                    `c`.`name`,
                    `c`.`code`,
                    `c`.`slug`
                FROM `category` AS `c`
                WHERE 1
                ORDER BY `c`.`name` ASC
            ', 'ECommerce/Models/Category', $options);

            $count = $this->select('
                SELECT COUNT(`c`.`id`) AS `count`
                FROM `category` AS `c`
                WHERE 1', null, $options, true);

            return [
                'count' => $count[0]['count'],
                'data' => $data
            ];
        }

        public function findByIdOrSlug(string $term)
        {
            return $this->find([
                'condition' => '`c`.`id` = :id OR `c`.`slug` = :slug',
                'value' => [
                    'id' => (int) $term,
                    'slug' => $term
                ],
                'limit' => 1,
                'offset' => 0
            ]);
        }

        public function findByName(string $name)
        {
            return $this->find([
                'condition' => '`c`.`name` = :name',
                'value' => [
                    'name' => $name
                ],
                'limit' => 1,
                'offset' => 0
            ]);
        }

        public function store(IModel $model)
        {
            if ($model->isNew()) {
                $id = $this->insert('
                    INSERT INTO `category`
                    (`name`,`code`,`slug`) VALUES
                    (:name, :code, :slug)', [
                        'name' => $model->getName(),
                        'code' => $model->getCode(),
                        'slug' => $model->getSlug()
                    ]);
                $model->setId($id);
            } else {
                $this->update('
                    UPDATE `category`
                        SET name = :name,
                            code = :code,
                            slug = :slug
                        WHERE id = :id
                        LIMIT 1', [
                        'name' => $model->getName(),
                        'code' => $model->getCode(),
                        'slug' => $model->getSlug(),
                        'id' => $model->getId()
                    ]);
            }

            return [
                'count' => 1,
                'data' => [$model]
            ];
        }

        public function delete(IModel $model)
        {
            return $this->remove('
                DELETE FROM `category`
                WHERE `id` = :id', [
                    'id' => $model->getId()
                ]);
        }
    }
}