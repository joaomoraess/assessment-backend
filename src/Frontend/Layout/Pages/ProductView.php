<script id="page-product-view" type="text/x-template">
    <div>
        <component-header></component-header>
        <main class="content">
            <h1 class="title new-item">{{product.name}}</h1>
            <div class="input-field">
                <?= __('Product SKU'); ?>: {{product.sku}}
            </div>
            <div class="input-field">
                <?= __('Product Price'); ?>: {{product.price | currency('<?= __('US$'); ?>', '<?= __('en-us'); ?>')}}
            </div>
            <div class="input-field">
                <?= __('Product Quantity'); ?>: {{product.quantity}}
            </div>
            <div class="input-field" v-if="product.categories.length > 0">
                <?= __('Product Categories'); ?>:<br>
                <ul>
                    <li v-for="category in product.categories">{{category.name}}</li>
                </ul>
            </div>
            <div class="input-field" v-if="product.description != ''">
                <?= __('Product Description'); ?>:
                <pre>{{product.description}}</pre>
            </div>
            <span v-if="product.images != undefined && product.images[0] != undefined">
                <img :src="'/uploads/product/image/R/' + product.images[0].name">
            </span>
        </main>
        <component-footer></component-footer>
    </div>
</script>