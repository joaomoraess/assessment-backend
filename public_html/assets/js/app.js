var depends = [];
depends.push('vue');
depends.push('vue-router');
depends.push('page-error');
depends.push('page-dashboard');
depends.push('page-product-create');
depends.push('page-product-view');
depends.push('page-category-create');
depends.push('page-products');
depends.push('page-categories');
depends.push('bus');
depends.push('v0');
depends.push('amp-fit-text');
depends.push('amp-sidebar');
depends.push('request');
depends.push('validate');


define(depends, function(
    Vue,
    VueRouter,
    PageError,
    PageDashboard,
    PageProductCreate,
    PageProductView,
    PageCategoryCreate,
    PageProducts,
    PageCategories) {

    Vue.use(VueRouter);

    var bus = new Vue();

    var routes = [
        {
            path: '*', component: PageError
        },{
            path: '/',
            component: PageDashboard,
            props: {
                bus: bus
            }
        },{
            path: '/products',
            component: PageProducts,
            props: {
                bus: bus
            }
        },{
            path: '/product/create',
            component: PageProductCreate,
            props: {
                bus: bus
            }
        },{
            path: '/product/:slug',
            component: PageProductView,
            props: {
                bus: bus
            }
        },{
            path: '/product/edit/:slug',
            component: PageProductCreate,
            props: {
                bus: bus
            }
        },{
            path: '/categories',
            component: PageCategories,
            props: {
                bus: bus
            }
        },{
            path: '/category/create',
            component: PageCategoryCreate,
            props: {
                bus: bus
            }
        },{
            path: '/category/edit/:slug',
            component: PageCategoryCreate,
            props: {
                bus: bus
            }
        }
    ];

    var router = new VueRouter({
        mode: 'history',
        routes: routes
    });
    
    var template = new Vue({
        router,
        el: '#app',
        data: function() {
            return {
                bus: bus
            };
        }
    });

});