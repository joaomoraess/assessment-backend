<?php

namespace ECommerce\Api\Models
{
    class ProductModel extends Model
    {
        protected $fields = [
            'id',
            'name',
            'sku',
            'price',
            'quantity',
            'description',
            'slug',
            'categories',
            'images',
        ];

        protected $validate = [
            'id' => [
                'type' => 'int',
                'size' => 11,
            ],
            'name' => [
                'type' => 'string',
                'format' => 'default',
                'size' => 255,
                'required' => true,
            ],
            'sku' => [
                'type' => 'string',
                'format' => 'default',
                'size' => 255,
                'required' => true,
            ],
            'price' => [
                'type' => 'float',
                'format' => 'default',
                'required' => true,
            ],
            'quantity' => [
                'type' => 'int',
                'format' => 'default',
                'size' => 5,
                'required' => true,
            ],
            'available' => [
                'type' => 'bool',
                'format' => 'default',
                'size' => 5,
            ],
            'description' => [
                'type' => 'string',
                'format' => 'default',
                'size' => 255,
                'required' => true,
            ],
            'slug' => [
                'type' => 'string',
                'format' => 'slug',
                'size' => 30,
                'populate' => ['name', 'sku']
            ],
            'categories' => [
                'type' => 'array',
                'format' => 'raw',
            ],
            'images' => [
                'type' => 'array',
                'format' => 'raw',
            ],
        ];

        public function modify()
        {
            $this->validate['name']['label'] = __('Product Name');
            $this->validate['sku']['label'] = __('Product SKU');
            $this->validate['price']['label'] = __('Product Price');
            $this->validate['quantity']['label'] = __('Product Quantity');
            $this->validate['description']['label'] = __('Product Description');
            $this->validate['categories']['label'] = __('Product Categories');
            $this->validate['images']['label'] = __('Product Images');
        }
    }
}