<?php

namespace ECommerce\Api\Repositories
{
    use ECommerce\Api\Interfaces\Models\IModel;

    class ProductsImagesRepository extends Repository
    {
        public function find(array $options = [])
        {
            $data = $this->select('
                SELECT
                    `pi`.`product_id`,
                    `pi`.`group`,
                    `pi`.`name`,
                    `pi`.`image`,
                    `pi`.`size`
                FROM `productImage` AS `pi`
                WHERE 1
                ORDER BY `pi`.`name` ASC
            ', null, $options);

            return [
                'count' => count($data),
                'data' => $data
            ];
        }

        public function store(IModel $model)
        {
            $id = $this->insert('
                INSERT INTO `productImage`
                (`product_id`,`group`, `name`, `image`, `size`) VALUES
                (:product_id, :group, :name, :image, :size)', [
                    'product_id' => $model->getProduct_Id(),
                    'group' => $model->getGroup(),
                    'name' => $model->getName(),
                    'image' => $model->getImage(),
                    'size' => $model->getSize(),
                ]);
            $model->setId($id);
        }

        public function findByProduct(int $product_id)
        {
            return $this->find([
                'condition' => '`pi`.`product_id` = :product_id',
                'distinct' => true,
                'fields' => [
                    '`pi`.`name`'
                ],
                'value' => [
                    'product_id' => $product_id
                ],
            ]);
        }

        public function deleteByProduct(IModel $model)
        {
            $data = $this->find([
                'condition' => '`pi`.`product_id` = :product_id',
                'distinct' => true,
                'fields' => [
                    '`pi`.`name`',
                    '`pi`.`image`'
                ],
                'value' => [
                    'product_id' => $model->getProduct_Id()
                ],
            ]);

            if($data['count'] > 0) {
                $this
                    ->getService('ECommerce/Services/ProductsImages')
                    ->deleteFiles($data['data']);
                    
                return $this->remove('
                    DELETE FROM `productImage`
                    WHERE `product_id` = :product_id', [
                        'product_id' => $model->getProduct_Id()
                    ]);
            }
        }

        public function imagePrint(string $size, string $name)
        {
            return $this->find([
                'condition' => '`pi`.`size` = :size AND `pi`.`name` = :name',
                'fields' => [
                    '`pi`.`image`'
                ],
                'value' => [
                    'size' => $size,
                    'name' => $name,
                ],
            ]);
        }
    }
}