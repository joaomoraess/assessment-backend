<?php

namespace ECommerce\Config\Providers
{
    use FcPhp\Di\Interfaces\IDi;
    use FcPhp\Provider\Interfaces\IProviderClient;

    class Request implements IProviderClient
    {
        /**
         * Method to configure Di in providers
         *
         * @param FcPhp\Di\Interfaces\IDi $di Di Instance
         * @return void
         */
        public function getProviders(IDi $di) :IDi
        {
            $di->set('FcPhp/Request', 'FcPhp\Request\Request', ['server' => $_SERVER]);
            return $di;
        }
    }
}