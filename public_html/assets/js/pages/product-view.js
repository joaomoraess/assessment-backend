var depends = [];

depends.push('vue');
depends.push('currency');
depends.push('component-header');
depends.push('component-footer');

define(depends, function(Vue) {
    return Vue.component('PageProductView', {
        template: '#page-product-view',
        props: {
            bus: {
                type: Object,
                required: false,
                default: null
            }
        },
        data: function() {
            return {
                product: {
                    image: null,
                    name: null,
                    description: null,
                    slug: null,
                    available: false,
                    value: null,
                    sku: null,
                    quantity: null,
                    categories: []
                }
            };
        },
        mounted: function() {
            var self = this;
            if (this.$route.params.slug) {
                this.request('get', 'product/' + this.$route.params.slug, function(count, data) {
                    self.product = data[0];
                });
            }
        }
    });
});
