require.config({
    baseUrl: '/assets/',
    paths: {
      
      // External
      'vue': 'node_modules/vue/dist/vue.min',
      'vue-router': 'node_modules/vue-router/dist/vue-router.min',
      'axios': 'node_modules/axios/dist/axios.min',
      'v-money': 'node_modules/v-money/dist/v-money',
      'accounting': 'node_modules/accounting/accounting.min',
      'vue-multiselect': 'node_modules/vue-multiselect/dist/vue-multiselect.min',
      'vuejs-paginate': 'node_modules/vuejs-paginate/dist/index',
      'v0': 'js/external/v0',
      'amp-fit-text': 'js/external/amp-fit-text-0.1',
      'amp-sidebar': 'js/external/amp-sidebar-0.1',

      // ECommerce app
      'app': 'js/app',
      
      // Helpers
      'bus': 'js/helpers/bus',
      'request': 'js/helpers/request',
      'validate': 'js/helpers/validate',

      // Filters
      'currency': 'js/filters/currency',

      // Components
      'modal-confirm': 'js/components/modal-confirm',
      'component-header': 'js/components/header',
      'component-footer': 'js/components/footer',
      'page-dashboard': 'js/pages/dashboard',
      'page-error': 'js/pages/error',
      'page-product-create': 'js/pages/product-create',
      'page-product-view': 'js/pages/product-view',
      'page-category-create': 'js/pages/category-create',
      'page-products': 'js/pages/products',
      'page-categories': 'js/pages/categories',
    },
    shim: {
        "vue": {
            "exports": "Vue"
        }
    },
    'urlArgs': 'up=' + (new Date()).getTime()
});

require(['app']);