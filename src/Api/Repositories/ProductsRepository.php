<?php

namespace ECommerce\Api\Repositories
{
    use ECommerce\Api\Interfaces\Models\IModel;

    class ProductsRepository extends Repository
    {
        public function find(array $options = [])
        {
            $pages = 0;
            $currentPage = isset($options['currentPage']) ? $options['currentPage'] : false;
            $perPage = isset($options['perPage']) ? $options['perPage'] : false;

            $count = $this->select('
                SELECT COUNT(`p`.`id`) AS `count`
                FROM `product` AS `p`
                WHERE 1', null, $options, true);
            $count = (int) $count[0]['count'];

            if($count > 0) {

                if ($currentPage && $perPage) {
                    $limit = $perPage;
                    $pages = ceil($count / $perPage);
                    $offset = ($perPage * $currentPage) - $perPage;

                    $options['limit'] = $limit;
                    $options['offset'] = $offset;
                }

                $data = $this->select('
                    SELECT
                        `p`.`id`,
                        `p`.`name`,
                        `p`.`sku`,
                        `p`.`price`,
                        `p`.`quantity`,
                        `p`.`available`,
                        `p`.`description`,
                        `p`.`slug`
                    FROM `product` AS `p`
                    WHERE 1
                    ORDER BY `p`.`name` ASC
                ', 'ECommerce/Models/Product', $options);

                $categoriesRepository = $this->getRepository('ECommerce/Repositories/ProductsCategories');
                $productImageRepository = $this->getRepository('ECommerce/Repositories/ProductsImages');

                foreach ($data as $key => $item) {
                    $modelProductCategory = $this->getModel('ECommerce/Models/ProductCategory');
                    $modelProductCategory->setProduct_Id($item->getId());
                    $categories = $categoriesRepository->findByProduct($modelProductCategory);
                    $data[$key]->setCategories($categories['data']);
                    
                    $images = $productImageRepository->findByProduct($item->getId());
                    $data[$key]->setImages($images['data']);
                }

                return [
                    'count' => $count,
                    'pages' => $pages,
                    'data' => $data
                ];
            }

            return [
                'count' => 0,
                'data' => []
            ];
        }

        public function findByIdOrSlug(string $term)
        {
            return $this->find([
                'condition' => '`p`.`id` = :id OR `p`.`slug` = :slug',
                'value' => [
                    'id' => (int) $term,
                    'slug' => $term
                ],
                'limit' => 1,
                'offset' => 0
            ]);
        }

        public function store(IModel $model)
        {
            if ($model->isNew()) {
                $id = $this->insert('
                    INSERT INTO `product`
                    (`name`,`sku`,`price`,`quantity`,`description`,`slug`) VALUES
                    (:name, :sku, :price, :quantity, :description, :slug)', [
                        'name' => $model->getName(),
                        'sku' => $model->getSku(),
                        'price' => $model->getPrice(),
                        'quantity' => $model->getQuantity(),
                        'description' => $model->getDescription(),
                        'slug' => $model->getSlug(),
                    ]);
                $model->setId($id);
            } else {
                $this->update('
                    UPDATE `product`
                        SET name = :name,
                            sku = :sku,
                            price = :price,
                            quantity = :quantity,
                            description = :description,
                            slug = :slug
                        WHERE id = :id
                        LIMIT 1', [
                        'name' => $model->getName(),
                        'sku' => $model->getSku(),
                        'price' => $model->getPrice(),
                        'quantity' => $model->getQuantity(),
                        'description' => $model->getDescription(),
                        'slug' => $model->getSlug(),
                        'id' => $model->getId(),
                    ]);
            }

            return [
                'count' => 1,
                'data' => [$model]
            ];
        }

        public function delete(IModel $model)
        {
            return $this->remove('
                DELETE FROM `product`
                WHERE `id` = :id', [
                    'id' => $model->getId()
                ]);
        }

        public function findBySku(string $sku, int $id = null)
        {
            $values = [];
            $condition = [];

            $values['sku'] = $sku;
            $condition[] = '`p`.`sku` = :sku';
            if(!empty($id)) {
                $values['id'] = $id;
                $condition[] = '`p`.`id` != :id';
            }
            return $this->find([
                'condition' => implode(' AND ', $condition),
                'value' => $values,
                'limit' => 1,
                'offset' => 0
            ]);
        }
    }
}