<script id="component-header" type="text/x-template">
    <header>
        <div class="go-menu">
            <a on="tap:sidebar.toggle">☰</a>
            <router-link to="/" class="link-logo"><img src="/assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></router-link>
        </div>
        <div class="right-box">
            <span class="go-title"><?= __('Administration Panel') ?></span>
        </div>
        <amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
            <div class="close-menu">
                <a on="tap:sidebar.toggle">
                    <img src="/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
                </a>
            </div>
            <router-link to="/"><img src="/assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></router-link>
            <div>
                <ul>
                    <li><router-link to="/categories" class="link-menu"><?= __('Categories') ?></router-link></li>
                    <li><router-link to="/products" class="link-menu"><?= __('Products') ?></router-link></li>
                </ul>
            </div>
        </amp-sidebar>
    </header>
</script>