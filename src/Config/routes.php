<?php

return [
    // Product image view
    'uploads' => [
        [
            'route' => 'product/image/{size}/{name}',
            'action' => 'ECommerce/Services/ProductsImages@imagePrint',
            'method' => 'GET',
        ],
    ],
    'api/v1' => [
        // Categories
        [
            'route' => 'categories',
            'action' => 'ECommerce/Services/Categories@findAll',
            'method' => 'GET',
        ],
        [
            'route' => 'category',
            'action' => 'ECommerce/Services/Categories@store',
            'method' => 'POST',
        ],
        [
            'route' => 'category/{term}',
            'action' => 'ECommerce/Services/Categories@store',
            'method' => 'PUT',
        ],
        [
            'route' => 'category/{term}',
            'action' => 'ECommerce/Services/Categories@delete',
            'method' => 'DELETE',
        ],
        [
            'route' => 'category/{term}',
            'action' => 'ECommerce/Services/Categories@findOne',
            'method' => 'GET',
        ],

        // Product Image
        [
            'route' => 'product/{term}/image',
            'action' => 'ECommerce/Services/Products@image',
            'method' => 'POST',
        ],

        // Products
        [
            'route' => 'products',
            'action' => 'ECommerce/Services/Products@findAll',
            'method' => 'GET',
        ],
        [
            'route' => 'product',
            'action' => 'ECommerce/Services/Products@store',
            'method' => 'POST',
        ],
        [
            'route' => 'product/{term}',
            'action' => 'ECommerce/Services/Products@store',
            'method' => 'PUT',
        ],
        [
            'route' => 'product/{term}',
            'action' => 'ECommerce/Services/Products@delete',
            'method' => 'DELETE',
        ],
        [
            'route' => 'product/{term}',
            'action' => 'ECommerce/Services/Products@findOne',
            'method' => 'GET',
        ],
    ]
];